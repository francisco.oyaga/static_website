FROM nginx:alpine

ARG BUILD_VERSION=0.0.0

COPY ./public /usr/share/nginx/html

RUN sed -i "s/\VERSION/${BUILD_VERSION}/" /usr/share/nginx/html/index.html
